#!/bin/sh

# Decay rate

for i in `seq 0 5`
do
    for s in bc bs bv cs cv sv
    do
        qsub test.sh T1k1 $s 2_${i}_${s} --gamma 0.0$i
    done
done
