#!/bin/sh

# Use

for i in 2 4 6 8
do
    for s in bc bs bv cs cv sv
    do
        qsub test.sh T6k1 $s 6_${i}_${s} --use 0.$i
    done
done
