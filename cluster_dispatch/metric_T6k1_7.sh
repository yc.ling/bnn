#!/bin/sh

# Tau_rec

for i in 400 800 1200 1600 2000
    do
    for s in bc bs bv cs cv sv
    do
        qsub metric.sh $s 7_${i}_${s}
    done
done
