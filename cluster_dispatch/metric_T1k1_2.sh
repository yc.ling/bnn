#!/bin/sh

# Decay rate

for i in `seq 0 5`
do
    for s in bc bs bv cs cv sv
    do
        qsub metric.sh $s 2_${i}_${s}
    done
done