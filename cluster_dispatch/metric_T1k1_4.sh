#!/bin/sh

for i in 100 250 500 1000 2000
do
    for s in bc bs bv cs cv sv
    do
        qsub metric.sh $s 4_${i}_${s}
    done
done
