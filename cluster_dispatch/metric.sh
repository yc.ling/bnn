#!/bin/sh

#$ -S /bin/sh
#$ -cwd
#$ -V
#$ -q machine3.q
#$ -pe OpenMP 1
#$ -N SIR

export OMP_NUM_THREADS=$NSLOTS 

python ../scripts/metric.py $1 $2
