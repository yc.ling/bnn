#!/bin/sh

#$ -S /bin/sh
#$ -cwd
#$ -V
#$ -q machine4.q
#$ -pe OpenMP 88
#$ -N mgrl

export OMP_NUM_THREADS=$NSLOTS 

python ../scripts/test.py $@
