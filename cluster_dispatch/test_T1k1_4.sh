#!/bin/sh

# Input units size

for i in 100 250 500 1000 2000
do
    for s in bc bs bv cs cv sv
    do
        qsub test.sh T1k1 $s 4_${i}_${s} --n_in $i
    done
done
