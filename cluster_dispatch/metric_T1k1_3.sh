#!/bin/sh

for i in 0 5 10 15 20
do
    for s in bc bs bv cs cv sv
    do
        qsub metric.sh $s 3_${i}_${s}
    done
done
