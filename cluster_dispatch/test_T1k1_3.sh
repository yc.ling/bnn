#!/bin/sh

# Lateral inhibition

for i in 0 5 10 15 20
do
    for s in bc bs bv cs cv sv
    do
        qsub test.sh T1k1 $s 3_${i}_${s} --w_inh $i
    done
done