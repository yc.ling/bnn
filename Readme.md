# Description

This is the project I worked on during my masters. It is mainly based on [the work by Asabuki and Fukai (2019)](https://www.biorxiv.org/content/10.1101/517888v2). The algorithm called SCEM (self-conditioned entropy minimization [^1]) was claimed to be superior than FastICA, a popular algorithm for blind source separation, in some cases. See **jupyter_scripts/scem_t1k1.ipynb** for a replication of the study.

That being said, the result of my master's research suggests that the performance of the algorithm is actually questionable since it fails to separate the signals once the combination of the source signals deviates from the one used in the original study. This project contains most of simulation setups I used which led me to the conclusion.

I tidied up the codes and uploaded it here, so other lab members who either agree or disagree with me may pick up where I left off.


[^1]: However, they have been working on the name, so it might be different after being formally published.

# Setup

1. Create a conda environment with the spec: `conda env create -n {envname} --file=env.yml`
2. Activate the environment: `conda activate {envname}`
3. Build the libraries with **setup.sh** in the **bnn** folder.

If your system doesn't have ffmpeg but you want to use the [museval library](https://github.com/sigsep/sigsep-mus-db) for performance evaluation, install it with `conda install ffmpeg`

# Usage

1. Use **scripts/input_mix_*.py** to generate mixed signals.
2. See **jupyter_scripts/scem_t1k1.ipynb** for an example.

Note: The api roughly follows the development guide of scikit-learn, but the package is still not complete enough to be used in scikit-learn pipelines.

## Cluster-related files

I have created some script files to streamline the simulation procedures.

In **scripts/**:

* **test.py**: distribute each simulation condition to each process.
* **metric.py**: evaluate the performance of simulation results.
* **gather.py**: combine multiple **metric.csv** files into one.

Only **scripts/gather.py** is designed to be invoked on the client side directly.

In **cluster_dispatch/**:

* **test.sh**, **metric.sh**: they will invoke their counterpart python scripts on the cluster. This is *cluster-specific and should be modified* if to be run on different clusters.
* **test_*.sh**, **metric_*.sh**: these files specify the simulation conditions and the evaluation of performances. Enable execution with `chmod +x ...` and execute them to dispatch simulations.

Follow these files if you'd like to create your own simulation conditions.

Note: The result of simulations will take up tons of disk space.

# Neural network models

Currently there are three implementations (variants) of SCEM:

* T1k1: Input with spike
* T2k1: Input with firing rate
* T6k1: With short-term plasticity proposed by [Tsodyks and Markram (1997)](https://doi.org/10.1073/pnas.94.2.719)

**To any lab member who is planning to create new implementations / variants:**

Each implementation is defined by multiple files (messy, I know, but this is the most elegant structure that I can think of):

* **bnn/scem/cpp/*.hpp** : header file
* **bnn/scem/cpp/*.cpp** : source file 
* **bnn/scem/*.pyx** : wrapper of C++ class 
* **bnn/scem/_*.py** : wrapper of wrapper

So, the proper procedure will be:

1. Copy these files and modify them accordingly.
2. Modify **bnn/scem/_scem_cy.pyx** and **bnn/scem/__init__.py** accordingly.

The codes should be self-explanatory. I'll add more explanation if anyone gets confused.

After the implementation, you should be able to include it with `from bnn.scem import xxx`, where **xxx** is the name of the implementation.

For simpler implementation examples, see the Dummy module and the Tommy module in **bnn/ref**.

## US14

This model is based on [the first figure in the work by Urbanczik and Senn (2014)](https://doi.org/10.1016/j.neuron.2013.11.030).

I decide to include this because this is where the SCEM originates from. This also works as a sanity check to the implementation of the neural network dynamics.

---