from cython cimport view
import numpy as np

cdef extern from "c/_common.h":
    void _generate_rng_c(char* rng, int seed) nogil
    int _get_rng_size_c(int seed) nogil
    
def _generate_rng(int seed):
    cdef int size = _get_rng_size_c(seed)
    cdef char[::1] rng = view.array(shape=(size,), itemsize=sizeof(char), format='c')
    _generate_rng_c(&(rng[0]), seed)
    return np.asarray(rng)