import os
import numpy as np
from scipy.io import wavfile

class Mixer():
    def __init__(self, fnames, nframe, start=0):
        self.fnames = fnames
        self.src = np.empty((nframe,len(fnames)), dtype=np.float32)
        for i,fname in enumerate(fnames):
            self.src[:,i] = wavfile.read(fname)[1][start:start+nframe]
            if i == 0:
                self.freq = wavfile.read(fname)[0]
        self.src -= self.src.mean(0)
        self.src /= abs(self.src).max(0)

    def save_src(self, dname, fname_prefix='src'):
        if not os.path.exists(dname):
            os.makedirs(dname, exist_ok=True)
            
        np.save(f"{dname}/{fname_prefix}.npy", self.src)
        for i in range(2):
            wavfile.write(f"{dname}/{fname_prefix}_{i}.wav",
                          self.freq, self.src[:,i].astype(np.float32))

        return

    def mix(self, n, q_low, q_high, gain, dname, fname_prefix='mixed'):
        
        q_l = np.linspace(q_low, q_high, n)
        
        if not os.path.exists(dname):
            os.makedirs(dname, exist_ok=True)

        for k,q in enumerate(q_l):
            theta = q*np.pi
            Q = np.array([[np.cos(theta), np.sin(theta)],
                          [np.sin(theta), np.cos(theta)]])
            mixed = self.src.dot(Q).astype(np.float32)
            mixed -= mixed.mean(0)
            mixed /= abs(mixed.max(0))
            for i in range(2):
                wavfile.write(f"{dname}/{fname_prefix}_{k}_{i}.wav",
                              self.freq, mixed[:,i].astype(np.float32))

            mixed = gain*(mixed + 1)/2
            np.save(f"{dname}/{fname_prefix}_{k}.npy", mixed)

        with open(f"{dname}/{fname_prefix}_rec.txt", 'w') as f:
            f.write(f"fnames: {self.fnames}\n"
                    f"n: {n}\n"
                    f"q_low: {q_low}\n"
                    f"q_high: {q_high}\n"
                    f"gain: {gain}\n")

        return
