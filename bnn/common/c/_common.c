#include "_common.h"
#include "mkl.h"

void _generate_rng_c(char* rng, int seed){
    VSLStreamStatePtr stream;
    vslNewStream(&stream, VSL_BRNG_MT19937, seed);
    vslSaveStreamM(stream, rng);
    vslDeleteStream(&stream);
    return;
}

int _get_rng_size_c(int seed){
    VSLStreamStatePtr stream;
    vslNewStream(&stream, VSL_BRNG_MT19937, seed);
    int size = vslGetStreamSize(stream);
    vslDeleteStream(&stream);
    return size;
}

void _phi_c(int n, float* x, float* y, float phi0, float beta, float theta){
    int i;
    cblas_scopy(n, x, 1, y, 1);
    for(i=0; i < n; ++i){
        y[i] -= theta;
    }
    cblas_sscal(n, beta, y, 1);
    vsTanh(n, y, y);
    for(i=0; i < n; ++i){
        y[i] += 1.0f;
    }
    cblas_sscal(n, phi0/2.0f, y, 1);
    return;
}

void _h_c(int n, float* x, float* y, float beta, float theta){
    int i;
    cblas_scopy(n, x, 1, y, 1);
    for(i=0; i < n; ++i){
        y[i] -= theta;
    }
    cblas_sscal(n, -beta, y, 1);
    vsTanh(n, y, y);
    for(i=0; i < n; ++i){
        y[i] += 1.0f;
    }
    cblas_sscal(n, beta/2.0f, y, 1);
    return;
}