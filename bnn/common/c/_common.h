#ifndef _COMMON_H
#define _COMMON_H

void _generate_rng_c(char*, int);

int _get_rng_size_c(int);

void _phi_c(int n, float* x, float* y, float phi0, float beta, float theta);

void _h_c(int n, float* x, float* y, float beta, float theta);

#endif 