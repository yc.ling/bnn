import time
import numpy as np
from numpy import sqrt

from sklearn.base import BaseEstimator, TransformerMixin

from ._scem_cy import _T1k1

class T1k1(BaseEstimator, TransformerMixin):
    """
    
    Parameters
    ----------
    N_in : int, default=1000
        Number of input units.
    N : int, default=2
        Number of output units.
    dt : float, default=1
        Size of the time step (ms).
        (DO NOT TOUCH THIS!!!)
    tau : float, default=15
        Membrane time constant.
    tau_syn : float, default=15/4
        Synaptic time constant.
    g_l : float, default=1/225
        Membrane conductance.
    g_d : float, default=2/3
        Dendritic conductance.
    gain : float, default=0.01
        Maximum output frequency (kHz).
    beta : float, default=5
        Output noisiness.
    theta : float, default=0.5
        Output threshold.
    eta : float, default=0.2
        Learning rate.
    gamma : float, default=3e-5
        Weight decay rate.
    w_size : int, default=1500
        Window size of somatic potential history.
    w_m : float, default=0
        Initial weight mean.
    w_s : float, default=0.25
        Initial weight variance (pre adjustment).
    w_inh : float, default=10
        Initial inhibitory weight.
    random_state : int or None, default=None
        Seed of MKL RNG. Use current timestamp if 'None'.
    
    Attributes
    ----------
    nn_ : _T1k1
        Actual instance.
    w_s_ : float
        Adjusted initial weight variance.
    eta_ : float
        Adjusted learning rate.
    gamma_ : float
        Adjusted weight decay rate.
    random_state_ : int
        Seed of MKL RNG. Set to the timestamp at execution if random_state is None. 
    idx_ : ndarray
        Correspondance between input units and input signals.
    t_rec_ : ndarray
        Timestamp of weight matrix snapshots in "one" iteration.
    w_rec_ : ndarray
        Snapshots of the weight matrix during fitting.
    
    """
    def __init__(self, n_in=1000, n=2, dt=1, tau=15, tau_syn=15/4,
                 g_l=1/225, g_d=2/3, gain=0.01, beta=5, theta=0.5,
                 eta=0.2, gamma=3e-5, w_size=1500, w_m=0, w_s=0.25,
                 w_inh=10, random_state=None):
        self.n_in = n_in
        self.n = n
        self.dt = dt
        self.tau = tau
        self.tau_syn = tau_syn
        self.g_l = g_l
        self.g_d = g_d
        self.gain = gain
        self.beta = beta
        self.theta = theta
        self.eta = eta
        self.gamma = gamma
        self.w_size = w_size
        self.w_m = w_m
        self.w_s = w_s
        self.w_inh = w_inh
        self.random_state = random_state
    
    def fit(self, X, y=None, niter=10, rec_t=None):
        """
        Parameters
        ----------
        X : ndarray 
            Input signals with dimension (nframe, nsrc).
            nframe is the number of frames.
            nsrc is the number of sources.
        y : (scikit-learn api requirement)
        niter : int, default=10
            Number of iterations
        rec_t : int or None, default=None
            Frame interval between weight matrix snapshots.
            If None, record the weight matrix at the beginning of each iteration, plus the weight matrix after training.  
        """
        self.w_s_ = sqrt(self.w_s/(self.n_in+self.n))
        self.eta_ = self.eta / self.n_in
        self.gamma_ = self.gamma * self.n_in
        
        if self.random_state is None:
            self.random_state_ = int(time.time())
        else:
            self.random_state_ = int(self.random_state)
        
        self.nn_ = _T1k1(int(self.n_in), int(self.n), 
                         float(self.w_m), float(self.w_s_), float(self.w_inh),
                         float(self.dt), float(self.tau), float(self.tau_syn), 
                         float(self.g_l), float(self.g_d),
                         float(self.gain), float(self.beta), float(self.theta), 
                         int(self.w_size), float(self.eta_), float(self.gamma_), 
                         int(self.random_state_))
        
        T = X.shape[0]
        nsrc = X.shape[1]

        self.idx_ = np.repeat(np.arange(nsrc), int(self.n_in/nsrc)).astype(np.int32)
        
        if rec_t is None:
            rec_t = T + 1
            
        nrec_per_iter = int(np.ceil(T/rec_t))
        nrec = int(niter*nrec_per_iter + 1)
        rec_size = int(self.n*(self.n_in+self.n))
        self.t_rec_ = np.arange(T, step=rec_t)
        self.w_rec_ = np.zeros((nrec, rec_size), dtype=np.float32)
        
        self.nn_.train(niter, X.astype(np.float32), self.idx_, rec_t, self.w_rec_)
        
        return self
    
    def transform(self, X, niter=10):
        """
        Parameters
        ----------
        X : ndarray 
            Input signals with dimension (nframe, nsrc).
            nframe is the number of frames.
            nsrc is the number of sources.
        niter : int, default=10
            Number of iterations
        """
        if not hasattr(self, 'nn_'):
            raise
        y = np.zeros((X.shape[0],self.n), dtype=np.float32)
        self.nn_.test(niter, X.astype(np.float32), self.idx_, y)
        
        return y