# distutils: language = c++

cdef extern from "cpp/t2k1.cpp":
    pass

cdef extern from "cpp/t2k1.hpp":
    cdef cppclass T2k1:
        T2k1(int N_in, int N, float w_m, float w_s, float w_inh,
             float dt, float tau, float tau_syn, float g_l, float g_d,
             float gain, float beta, float theta,
             int w_size, float eta, float gamma, int seed) except +
        void train(int ntrain, int nframe, int nsrc, float *nu_src, int *idx, int rec_T, float *w_rec) except +
        void test(int ntest, int nframe, int nsrc, float *nu_src, int *idx, float *est) except +

cdef class _T2k1:
    cdef T2k1 *obj
    
    def __cinit__(self, int N_in, int N, float w_m, float w_s, float w_inh,
                  float dt, float tau, float tau_syn, float g_l, float g_d,
                  float gain, float beta, float theta,
                  int w_size, float eta, float gamma, int seed):
        self.obj = new T2k1(N_in, N, w_m, w_s, w_inh,
                            dt, tau, tau_syn, g_l, g_d,
                            gain, beta, theta,
                            w_size, eta, gamma, seed)
    
    def __dealloc__(self):
        del self.obj

    def train(self, int ntrain, float[:,::1] nu_src, int[::1] idx, int rec_T, float[:,::1] w_rec):
        self.obj.train(ntrain, nu_src.shape[0], nu_src.shape[1], &(nu_src[0,0]), &(idx[0]), rec_T, &(w_rec[0,0]))

    def test(self, int ntest, float[:,::1] nu_src, int[::1] idx, float[:,::1] est):
        self.obj.test(ntest, nu_src.shape[0], nu_src.shape[1], &(nu_src[0,0]), &(idx[0]), &(est[0,0]))
