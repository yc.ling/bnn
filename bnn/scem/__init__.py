from ._t1k1 import T1k1
from ._t2k1 import T2k1
from ._t6k1 import T6k1

__all__ = ['T1k1', 'T2k1', 'T6k1']