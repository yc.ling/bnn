#include "t1k1.hpp"
#include "mkl.h"
#include <algorithm>
#include <string.h>
#include <vector>

T1k1::T1k1(int N_in, int N, float w_m, float w_s, float w_inh_0,
           float dt, float tau, float tau_syn, float g_l, float g_d,
           float gain, float beta, float theta,
           int w_size, float eta, float gamma, int seed) : 
    N_in(N_in), N(N), dt(dt),
    dec_Isyn(1-dt/tau_syn), dec_PSP(1-dt/tau),
    inc_Vsom(dt*g_d), dec_Vsom(1-dt*(g_l+g_d)), 
    c_Vstar(g_d/(g_d+g_l)),  
    gain(gain), beta(beta), theta(theta),
    inc_w(dt*eta), dec_w(1-dt*eta*gamma), w_size(w_size),
    I_syn(N_in, 0), PSP(N_in, 0),
    V_dnd(N, 0), V_som(N, 0), nu(N, 0),
    V_star(N, 0), nu_star(N, 0),
    V_som_list(w_size*N, 0), V_som_avg(N, 0), V_som_var(N, 0),
    fbuf(std::max(N_in, N), 0), fbuf2(std::max(N_in, N), 0),
    w(N*N_in, 0), w_inh(N*N, 0)
{
    vslNewStream(&stream, VSL_BRNG_WH, seed);
    vsRngGaussian(VSL_RNG_METHOD_GAUSSIAN_ICDF, stream, N*N_in, w.data(), w_m, w_s);

    for(int i = 0; i < N; i++){
        for(int j = 0; j < N; j++){
            if(i != j) w_inh[N*i+j] = w_inh_0;
        }
    }
}

T1k1::~T1k1()
{
    vslDeleteStream(&stream);
}

void T1k1::reset_neuron()
{
    std::fill(I_syn.begin(), I_syn.end(), 0);
    std::fill(PSP.begin(), PSP.end(), 0);
    std::fill(V_dnd.begin(), V_dnd.end(), 0);
    std::fill(V_som.begin(), V_som.end(), 0);
    std::fill(nu.begin(), nu.end(), 0);
    std::fill(V_star.begin(), V_star.end(), 0);
    std::fill(nu_star.begin(), nu_star.end(), 0);
}

void T1k1::reset_Vsom_list()
{
    std::fill(V_som_list.begin(), V_som_list.end(), 0);
    std::fill(V_som_avg.begin(), V_som_avg.end(), 0);
    std::fill(V_som_var.begin(), V_som_var.end(), 0);
    vSomPtr = 0;
}

void T1k1::step(const float* nu_src, const int* idx, const int k)
{
    vsRngUniform(VSL_RNG_METHOD_UNIFORM_STD, stream, N_in, fbuf.data(), 0, 1);
    for(int i = 0; i < N_in; i++)
        I_syn[i] += (fbuf[i] < dt*nu_src[idx[i]]);

    // PSP[k] ~ PSP[k-1], I_syn[k-1]
    cblas_saxpby(N_in, 1, I_syn.data(), 1, dec_PSP, PSP.data(), 1);
  
    // I_syn[k] ~ I_syn[k-1]
    cblas_sscal(N_in, dec_Isyn, I_syn.data(), 1);

    // V_som[k] ~ V_som[k-1], V_dnd[k-1], nu[k-1]
    cblas_saxpby(N, inc_Vsom, V_dnd.data(), 1, dec_Vsom, V_som.data(), 1);
    cblas_sgemv(CblasRowMajor, CblasNoTrans, N, N,
                -inc_Vsom, w_inh.data(), N, nu.data(), 1,
                1, V_som.data(), 1);
  
    // V_dnd[k] ~ w[k], PSP[k]
    cblas_sgemv(CblasRowMajor, CblasNoTrans, N, N_in,
                1, w.data(), N_in, PSP.data(), 1, 0, V_dnd.data(), 1);
  
    // V_star[k] ~ V_dnd[k]
    cblas_scopy(N, V_dnd.data(), 1, V_star.data(), 1);
    cblas_sscal(N, c_Vstar, V_star.data(), 1);

    // nu_star[k] ~ V_star[k]
    g(V_star, nu_star);

    float *old_V_som = &(V_som_list[vSomPtr]);
  
    // V_som_avg[k]
    cblas_scopy(N, V_som_avg.data(), 1, fbuf2.data(), 1);
    vsSub(N, V_som.data(), old_V_som, fbuf.data());
    cblas_saxpy(N, 1.0/w_size, fbuf.data(), 1, V_som_avg.data(), 1);
  
    // V_som_var[k]
    vsSub(N, V_som.data(), fbuf2.data(), fbuf2.data());
    vsSub(N, fbuf2.data(), V_som_avg.data(), fbuf2.data());
    vsAdd(N, fbuf2.data(), old_V_som, fbuf2.data());
    vsMul(N, fbuf.data(), fbuf2.data(), fbuf.data());
    cblas_saxpy(N, 1.0/(w_size-1), fbuf.data(), 1, V_som_var.data(), 1);

    // V_som_list[k]
    cblas_scopy(N, V_som.data(), 1, old_V_som, 1);
    vSomPtr += N;
    if(vSomPtr == w_size*N) vSomPtr = 0;

    // nu[k] ~ V_som[k]
    if(k == 1) g(V_som, nu);
}

void T1k1::update_w()
{
    // nu[k](V_som_white)
    vsSub(N, V_som.data(), V_som_avg.data(), fbuf.data());
    vsSqrt(N, V_som_var.data(), fbuf2.data());
    vsDiv(N, fbuf.data(), fbuf2.data(), fbuf.data());
    g(fbuf, nu);

    vsSub(N, nu.data(), nu_star.data(), fbuf.data());
    h(V_star, fbuf2);
    vsMul(N, fbuf.data(), fbuf2.data(), fbuf.data());
    cblas_sscal(N*N_in, dec_w, w.data(), 1);
    cblas_sger(CblasRowMajor, N, N_in,
               inc_w, fbuf.data(), 1, PSP.data(), 1, w.data(), N_in);
}

void T1k1::train(int ntrain, int nframe, int nsrc, const float* nu_src, const int* idx, int rec_T, float* w_rec)
{
    reset_Vsom_list();

    int row = 0;
    int rec_size = N*(N_in+N);
    int rec_i_offset = N*N_in;
    for(int t = 0; t < ntrain; t++){
        reset_neuron();
    
        for(int k = 0; k < nframe; k++){
            if(rec_T != 0 && k%rec_T == 0){
                cblas_scopy(N*N_in, w.data(), 1, &(w_rec[row*rec_size]), 1);
                cblas_scopy(N*N, w_inh.data(), 1, &(w_rec[row*rec_size+rec_i_offset]), 1);
                row++;
            }
            step(&(nu_src[k*nsrc]), idx, 0);
            if(k > w_size) update_w();
        }
    }
    cblas_scopy(N*N_in, w.data(), 1, &(w_rec[row*rec_size]), 1);
    cblas_scopy(N*N, w_inh.data(), 1, &(w_rec[row*rec_size+rec_i_offset]), 1);
}

void T1k1::test(int ntest, int nframe, int nsrc, const float *nu_src, const int *idx, float *est)
{
    memset(est, 0, sizeof(float)*nframe*N);

    for(int t = 0; t < ntest; t++){
        reset_neuron();
        
        for(int k = 0; k < nframe; k++){
            step(&(nu_src[k*nsrc]), idx, 1);
            cblas_saxpy(N, 1.0/ntest, nu.data(), 1, &(est[k*N]), 1);
        }
    }
}
