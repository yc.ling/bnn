#ifndef T2K1_H
#define T2K1_H

#include "mkl.h"
#include <cmath>
#include <vector>

class T2k1{
private:
    const int N_in, N;
    const float dt;
    const float dec_Isyn, dec_PSP;
    const float inc_Vsom, dec_Vsom;
    const float c_Vstar;
    const float gain, beta, theta;
    const float inc_w, dec_w;
    const int w_size;

    std::vector<float> I_syn;
    std::vector<float> PSP;
    std::vector<float> V_dnd;
    std::vector<float> V_som;
    std::vector<float> nu;

    std::vector<float> V_star;
    std::vector<float> nu_star;
  
    std::vector<float> V_som_list;
    std::vector<float> V_som_avg;
    std::vector<float> V_som_var;
    int vSomPtr;

    std::vector<float> w;
    std::vector<float> w_inh;
  
    VSLStreamStatePtr stream;  
    std::vector<float> fbuf;
    std::vector<float> fbuf2;

    void g(const std::vector<float>& x, std::vector<float>& y){
        for(int i = 0; i < N; i++){
            y[i] = gain*(1+std::tanh(beta*(x[i]-theta)/2))/2;
        }
    }

    void h(const std::vector<float>& x, std::vector<float>& y){
        for(int i = 0; i < N; i++){
            y[i] = beta*(1+std::tanh(beta*(theta-x[i])/2))/2;
        }
    }

    void reset_neuron();
    void reset_Vsom_list();
    void step(const float* nu_src, const int* idx, const int k);
    void update_w();

public:
    T2k1(int N_in, int N, float w_m, float w_s, float w_inh,
        float dt, float tau, float tau_syn, float g_l, float g_d,
        float gain, float beta, float theta, 
        int w_size, float eta, float gamma, int seed);

    ~T2k1();

    void train(int ntrain, int nframe, int nsrc, const float* nu_src, const int* idx, int rec_T, float* w_rec);
      void test(int ntest, int nframe, int nsrc, const float *nu_src, const int *idx, float *est);
};

#endif
