import os
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
from Cython.Distutils import build_ext

conda_env = os.environ.get('CONDA_PREFIX')

ext_modules = [Extension("bnn.ref._dummy_cy",
                         ["ref/_dummy_cy.pyx", "ref/c/_dummy.c"],
                         include_dirs=[f'{conda_env}/include'],
                         libraries=["mkl_rt"],
                         library_dirs=[f'{conda_env}/lib'],
                         extra_compile_args=["-O3", "-fopenmp"]),
               Extension("bnn.ref._tommy_cy",
                         ["ref/_tommy_cy.pyx"],
                         include_dirs=[f'{conda_env}/include'],
                         libraries=["mkl_rt"],
                         library_dirs=[f'{conda_env}/lib'],
                         extra_compile_args=["-O3", "-fopenmp"]),
               Extension("bnn.ref._us14_cy",
                         ["ref/_us14_cy.pyx", "ref/c/_us14.c",
                          "common/c/_common.c"],
                         include_dirs=[f'{conda_env}/include', '.'],
                         libraries=["mkl_rt"],
                         library_dirs=[f'{conda_env}/lib'],
                         extra_compile_args=["-O3", "-fopenmp"]),
               Extension("bnn.common._common_cy",
                         ["common/_common_cy.pyx", "common/c/_common.c"],
                         include_dirs=[f'{conda_env}/include'],
                         libraries=["mkl_rt"],
                         library_dirs=[f'{conda_env}/lib'],
                         extra_compile_args=["-O3", "-fopenmp"]),
               Extension("bnn.scem._scem_cy",
                         ["scem/_scem_cy.pyx"],
                         include_dirs=[f'{conda_env}/include'],
                         libraries=["mkl_rt"],
                         library_dirs=[f'{conda_env}/lib'],
                         extra_compile_args=["-O3", "-fopenmp"]),

]

setup(name = "bnn",
      packages = ['bnn.ref', 'bnn.common', 'bnn.scem'],
      package_dir = {
          'bnn.ref' : 'ref',
          'bnn.common' : 'common',
          'bnn.scem' : 'scem',
      },
      ext_modules = cythonize(ext_modules,
                              compiler_directives={'boundscheck': False,
                                                   'wraparound': False,
                                                   'language_level': 3}),
      cmdclass = {'build_ext': build_ext})
