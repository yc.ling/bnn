# distutils: language = c++
from cython cimport view
import numpy as np

cdef extern from "cpp/_tommy.cpp":
    pass
 
cdef extern from "cpp/_tommy.hpp":
    cdef cppclass Tommy:
        Tommy(float param) except +
        void fit(int n, float* X, float* y)
        void predict(int n, float* X, float* y)
        
cdef class _Tommy:
    cdef Tommy *tommy
    
    def __cinit__(self, float param):
        self.tommy = new Tommy(param)
        
    def __dealloc__(self):
        del self.tommy
    
    def fit(self, float[::1] X, float[::1] y):
        return self.tommy.fit(X.shape[0], &(X[0]), &(y[0]))

    def predict(self, float[::1] X):
        cdef float[:] y = np.zeros(X.shape[0], dtype=np.float32)
        self.tommy.predict(X.shape[0], &(X[0]), &(y[0]))
        return np.asarray(y)