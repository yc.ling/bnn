#ifndef _US14_H
#define _US14_H

void _fit_c(int T, int N, float* X, float* g_e, float* g_i, float* w,
            float* i_dnd, float* psp, float* delta, float* pl,
            float* v_w, float* v_w_star, float* u, float* s,
            float dt, float tau_l, float tau_s, float g_d, float g_l,
            float e_e, float e_i, float phi_max, float beta, float theta,
            float tau_delta, float eta, char* rng);

void _init_w_c(int n, float* w, float w0, char* rng);

#endif