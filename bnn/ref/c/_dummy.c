#include "_dummy.h"
#include <stdlib.h>
#include "mkl.h"

void _fit_c(float* arr, int n){
    vsExp(n, arr, arr);
    return;
}

void _random_c(char* rng, int n, float* buf){
    VSLStreamStatePtr stream;
    vslLoadStreamM(&stream, rng);
    vsRngGaussian(VSL_RNG_METHOD_GAUSSIAN_BOXMULLER, stream, n, buf, 0, 1);
    vslSaveStreamM(stream, rng);
    vslDeleteStream(&stream);
}