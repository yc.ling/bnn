#include "_us14.h"
#include "mkl.h"
#include "common/c/_common.h"

void _fit_c(int T, int N, float* X, float* g_e, float* g_i, float* w,
            float* i_dnd, float* psp, float* delta, float* pl,
            float* v_w, float* v_w_star, float* u, float* s,
            float dt, float tau_l, float tau_s, float g_d, float g_l,
            float e_e, float e_i, float phi_max, float beta, float theta, 
            float tau_delta, float eta, char* rng){
    VSLStreamStatePtr stream;
    vslLoadStreamM(&stream, rng);
    
    float alpha_l = dt/tau_l;
    float beta_l = 1.0f - alpha_l;
    float alpha_s = 1.0f/tau_s;
    float beta_s = 1.0f - dt/tau_s;
    float alpha_eta = dt*eta;
    float alpha_u = dt*g_d;
    float beta_u = 1.0f - dt*(g_l+g_d);
    float alpha_star = g_d / (g_l+g_d);
    float alpha_delta = 1.0f/tau_delta;
    float beta_delta = 1.0f - dt/tau_delta;
    float i_som;
    float phi0 = phi_max*dt;
    
    int k;
    float f;
    float buf;
    for(k = 0; k < T; ++k){
        cblas_saxpy(N, alpha_s, &(X[k*N]), 1, i_dnd, 1);
        cblas_saxpby(N, alpha_l, i_dnd, 1, beta_l, psp, 1),
        cblas_sscal(N, beta_s, i_dnd, 1);
        v_w[k+1] = cblas_sdot(N, w, 1, psp, 1);

        i_som = g_e[k]*(e_e-u[k]) + g_i[k]*(e_i-u[k]);
        u[k+1] = beta_u*u[k] + alpha_u*v_w[k] + dt*i_som;
        
        _phi_c(1, &(u[k+1]), &f, phi0, beta, theta);
        vsRngUniform(VSL_RNG_METHOD_UNIFORM_STD, stream, 1, &buf, 0, 1);
        s[k+1] = (buf < f);
        
        v_w_star[k+1] = alpha_star*v_w[k+1];
        cblas_scopy(N, psp, 1, pl, 1);
        _h_c(1, &(v_w_star[k+1]), &buf, beta, theta);
        _phi_c(1, &(v_w_star[k+1]), &f, phi0, beta, theta);
        cblas_sscal(N, (s[k+1]-f)*buf, pl, 1);
        
        cblas_saxpy(N, alpha_delta, pl, 1, delta, 1);
        cblas_saxpy(N, alpha_eta, delta, 1, w, 1);
        cblas_sscal(N, beta_delta, delta, 1);
    }        
    
    vslSaveStreamM(stream, rng);
    vslDeleteStream(&stream);
    return;
}

void _init_w_c(int n, float* w, float w0, char* rng){
    VSLStreamStatePtr stream;
    vslLoadStreamM(&stream, rng);
    vsRngGaussian(VSL_RNG_METHOD_GAUSSIAN_BOXMULLER, stream, n, w, w0, 2*w0);
    vslSaveStreamM(stream, rng);
    vslDeleteStream(&stream);
    return;
}