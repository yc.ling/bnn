import numpy as np

from sklearn.base import BaseEstimator

from ._tommy_cy import _Tommy

class Tommy(BaseEstimator):
    def __init__(self, param):
        self.param = param
        pass
        
    def fit(self, X, y):
        self.soul_ = _Tommy(self.param)
        
        self.soul_.fit(X.astype(np.float32), y.astype(np.float32))
        
        return self
        
    def predict(self, X):
        if not hasattr(self, 'soul_'):
            raise
            
        return self.soul_.predict(X.astype(np.float32))