import numpy as np

from sklearn.base import BaseEstimator
from sklearn.utils.validation import check_X_y, check_array, check_random_state

from ._dummy_cy import _fit, _random
from ..common._common_cy import _generate_rng

class Dummy(BaseEstimator):
    def __init__(self):
        pass
    
    def generate_rng(self, seed):
        self.rng_ = _generate_rng(seed)
        return
    
    def random(self, n):
        return _random(self.rng_, n)
        
    def fit(self, X, y):
        self.arr_ = np.array([1,2,3]).astype(np.float32)
        _fit(self.arr_)
        
        return self
        
    def predict(self, X):
        return self.arr_