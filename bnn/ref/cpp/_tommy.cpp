#include "_tommy.hpp"
#include <vector>
#include <iostream>

Tommy::Tommy(float param) :
    param(param), X(0,0), y(0,0)
{}

Tommy::~Tommy(){}

void Tommy::fit(int n, float* X, float* y){    
    this->X.assign(X, X+n);
    this->y.assign(y, y+n);
    return;
}

void Tommy::predict(int n, float* X, float* y){
    for(int i=0; i < n; ++i){
        y[i] = X[i] + this->X[i] + this->y[i];
    }
    return;
}