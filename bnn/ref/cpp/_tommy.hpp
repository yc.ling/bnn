#ifndef _TOMMY_HPP
#define _TOMMY_HPP

#include <vector>

class Tommy {
    private:
        float param;
        std::vector<float> X;
        std::vector<float> y;
    public:
        Tommy(float param);
        ~Tommy();
        void fit(int n, float* X, float* y);
        void predict(int n, float* X, float* y);
};

#endif