import time
import numpy as np
from numpy import exp, tanh

from sklearn.base import BaseEstimator
from sklearn.utils.validation import check_X_y, check_array, check_random_state

from ..common._common_cy import _generate_rng
from ._us14_cy import _fit, _init_w

class US14(BaseEstimator):
    """
    
    Reference
    ---------
    Title: Learning by the dendritic prediction of somatic spiking.
    Author: R. Urbanczik, W. Senn.
    DOI: http://dx.doi.org/10.1016/j.neuron.2013.11.030
    
    Parameters
    ----------
    phi_max : float, default=0.15
        Maximum of the activation function.
    beta : float, default=5
        Sensitivity of the activation function.
    theta : float, default=0.86
        Soft threshold of the activation function.
    tau_s : float, default=3
        Synaptic time constant of the dendritic compartment.
    tau_l : float, default=10
        Leak time constant of the dendritic compartment.
    g_l : float, default=0.1
        Leak conductance of the somatic compartment.
    g_d : float, default=2
        Dendritic conductance of the somatic compartment.
    e_e : float, default=14/3
        Excitatory reversal potential.
    e_i : float, default=-1/3
        Inhibitory reversal potential.
    w_0 : float, default=0.2
        Mean value of initial weight.
    tau_delta : float, default=100
        Time constant of delta.
    eta : float, default=0.07
        Learning rate.
    warm_start: bool, default=True
        (Dummy variable)
    random_state: int or None, default=None
        Seed of dedicated rng.
    
    Attributes
    ----------
    n_in_ : int
        The number of input signals.
    w_ : ndarray
        Weight matrix.
    i_dnd_ : ndarray
        Input current.
    psp_ : ndarray
        Post-synaptic potential.
    v_w_ : ndarray
        Dentritic potential history corresponding to the last input.
    v_w_star_ : ndarray
        Dendritic prediction of somatic potential
    u_ : ndarray
        Somatic potential history corresponding to the last input.
    s_ : ndarray
        Output spike train.
    g_e_ : ndarray
        Excitatory nudging conductance.
    g_i_ : ndarray
        Inhibitory nudging conductance.
    delta_ : ndarray
        Error buffer.
    pl_ : ndarray
        Plasticity induction variable.
    rng_ : np.random.RandomState
        RandomState instance.
    
    """
    
    def __init__(self, phi_max=0.15, beta=5, theta=0.86,
                 tau_s=3, tau_l=10, g_l=0.1, g_d=2, e_e=14/3, e_i=-1/3, 
                 w_0=0.2, tau_delta=100, eta=0.07, warm_start=True, 
                 random_state=None):
        
        self.phi_max = phi_max
        self.beta = beta
        self.theta = theta
        self.tau_s = tau_s
        self.tau_l = tau_l
        self.g_l = g_l
        self.g_d = g_d
        self.e_e = e_e
        self.e_i = e_i     
        self.w_0 = w_0
        self.tau_delta = tau_delta
        self.eta = eta
        self.random_state = random_state
        self.warm_start = True
        
    def _phi(self, x):
        return self.phi_max * (1+tanh(self.beta*(x-self.theta))) / 2
    
    def _h(self, x):
        return self.beta * (1+tanh(self.beta*(self.theta-x))) / 2
    
    def fit(self, X, y=None, dt=1):
        if y is None:
            X = check_array(X)
        else:
            pass
            # allow nan in y
            # X, y = check_X_y(X, y) 
        
        if not hasattr(self, 'n_in_'):
            self.n_in_ = X.shape[1]
            self.i_dnd_ = np.zeros(self.n_in_, dtype=np.float32)
            self.psp_ = np.zeros(self.n_in_, dtype=np.float32)
            self.delta_ = np.zeros(self.n_in_, dtype=np.float32)
            self.pl_ = np.zeros(self.n_in_, dtype=np.float32)
            self.v_w_ = np.zeros(X.shape[0]+1, dtype=np.float32)
            self.v_w_star_ = np.zeros(X.shape[0]+1, dtype=np.float32)
            self.u_ = np.zeros(X.shape[0]+1, dtype=np.float32)
            self.s_ = np.zeros(X.shape[0]+1, dtype=np.float32)
            
            if self.random_state is None:
                self.random_state_ = int(time.time())
            else:
                self.random_state_ = int(self.random_state)
            self.rng_ = _generate_rng(self.random_state_)
            self.w_ = np.zeros(self.n_in_, dtype=np.float32)
            _init_w(self.w_, self.w_0, self.rng_)
            
        elif X.shape[1] != self.n_in_:
            raise ValueError(f"Number of input signals is {X.shape[1]}, "
                             f"expects {self.n_in_}.")        
        
        elif X.shape[0] != self.v_w_.shape[0]:
            for arr in [self.v_w_, self.v_w_star_, self.u_, self.s_]:
                arr[0] = arr[-1]
                arr.resize(X.shape[0]+1)
        
        # Transform target signal into nudges
        if y is None:
            self.g_i_ = np.zeros(X.shape[0], dtype=np.float32)
            self.g_e_ = np.zeros(X.shape[0], dtype=np.float32)
        else:
            idx = np.isnan(y) # nan in target signal indicates no teaching 
            self.g_i_ = 2.0*np.ones_like(y, dtype=np.float32)
            self.g_i_[idx] = 0
            self.g_e_ = np.zeros_like(y, dtype=np.float32)
            self.g_e_[~idx] = (self.e_i-y[~idx])*self.g_i_[~idx]/(y[~idx]-self.e_e)
         
        _fit(X.astype(np.float32), self.g_e_, self.g_i_, self.w_,
            self.i_dnd_, self.psp_, self.delta_, self.pl_,
            self.v_w_, self.v_w_star_, self.u_, self.s_,
            float(dt), float(self.tau_l), float(self.tau_s), float(self.g_d), float(self.g_l),
            float(self.e_e), float(self.e_i), float(self.phi_max), float(self.beta), float(self.theta),
            float(self.tau_delta), float(self.eta), self.rng_)
        
            
        return self
    
    
    def predict(self, X, dt=1):
        # For this model, learning only occurs when teaching signal are present.
        # In other words, "predict" corresponds to "fit" without "y".
        self.fit(X, dt=1)
        
        return self.s_