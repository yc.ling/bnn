from libc.math cimport exp

cdef extern from "c/_us14.h":
    void _fit_c(int T, int N, float* X, float* g_e, float* g_i, float* w,
                float* i_dnd, float* psp, float* delta, float* pl,
                float* v_w, float* v_w_star, float* u, float* s, 
                float dt, float tau_l, float tau_s, float g_d, float g_l,
                float e_e, float e_i, float phi_max, float beta, float theta,
                float tau_delta, float eta, char* rng) nogil
    void _init_w_c(int n, float* w, float w0, char* rng) nogil
    
cpdef void _init_w(float[::1] w, float w0, char[::1] rng) nogil:
    _init_w_c(w.shape[0], &(w[0]), w0, &(rng[0]))
    return
    
cpdef void _fit(float[:,::1] X, float[::1] g_e, float[::1] g_i, float[::1] w,
                float[::1] i_dnd, float[::1] psp,
                float[::1] delta, float[::1] pl,
                float[::1] v_w, float[::1] v_w_star,
                float[::1] u, float[::1] s,
                float dt, float tau_l, float tau_s, float g_d, float g_l,
                float e_e, float e_i, float phi_max, float beta, float theta,
                float tau_delta, float eta, char[::1] rng) nogil:
    _fit_c(X.shape[0], X.shape[1], &(X[0,0]),
           &(g_e[0]), &(g_i[0]), &(w[0]), &(i_dnd[0]), &(psp[0]),
           &(delta[0]), &(pl[0]), &(v_w[0]), &(v_w_star[0]),
           &(u[0]), &(s[0]), dt, tau_l, tau_s, g_d, g_l, e_e, e_i, phi_max, beta, theta,
           tau_delta, eta, &(rng[0]))
    return