from cython cimport view
import numpy as np

cdef extern from "c/_dummy.h":
    void _fit_c(float* arr, int n) nogil
    void _random_c(char* rng, int n, float* buf) nogil
    
cpdef void _fit(float[::1] arr) nogil:
    _fit_c(&(arr[0]), arr.shape[0])
    return 

def _random(char[::1] rng, int n):
    cdef float[::1] buf = np.zeros(n, dtype=np.float32)
    _random_c(&(rng[0]), n, &(buf[0]))
    return np.asarray(buf)