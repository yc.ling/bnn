import os
import sys

from matplotlib import pyplot as plt

module_path = os.path.abspath(os.path.join('..'))
if module_path not in sys.path:
    sys.path.append(module_path)

from bnn.common import Mixer

def type1(dname, instr_l):
    fnames = [f"../data/Bach10/01-AchGottundHerr/01-AchGottundHerr-{instr}.wav" \
              for instr in instr_l]
    
    nframe = 50000*10
    start = 65000

    mixer = Mixer(fnames, nframe, start)
    mixer.save_src(dname)

    nq = 11
    q_low = 0
    q_high = 0.25
    gain = 0.01
    mixer.mix(nq, q_low, q_high, gain, dname)
            
    return
