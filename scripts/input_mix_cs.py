import input_mix

def main():
    input_mix.type1(
        "../input/cs",
        ["clarinet", "saxphone"])            
    return

if __name__ == "__main__":
    main()