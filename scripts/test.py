import argparse
import os, sys
import importlib
import re
import glob
import itertools
from multiprocessing import Pool
from scipy.io import wavfile
import numpy as np
    
def func(Model, kargs, seed, input_file, output_dir, trial_num, ntrain, ntest):
    kargs['random_state'] = seed
    model = Model(**kargs)
    input_num = re.search('mixed_(.+?).npy', input_file)[1]
    nu_in = np.load(input_file)

    model.fit(nu_in, niter=ntrain)
    np.save(f'{output_dir}/w_{input_num}_{trial_num}.npy', model.w_rec_)

    est = model.transform(nu_in, niter=ntest)
    np.save(f'{output_dir}/est_{input_num}_{trial_num}.npy', est)
    est -= est.mean(0)
    est /= abs(est).max(0)
    
    for i in range(est.shape[1]):
        wavfile.write(f'{output_dir}/est_{input_num}_{trial_num}_{i}.wav',
                      44100, est[:,i].astype(np.float32))

def main():
    ## Parsing
    parser = argparse.ArgumentParser()
    parser.add_argument('model',
                        help="Name of the model")
    parser.add_argument('input_dir',
                        help="Name of the subdirectory in the 'input' directory")
    parser.add_argument('output_dir',
                        help="Name of the subdirectory in the 'output' directory")
    
    parser.add_argument('--ntrial',
                        type=int,
                        default=10,
                        help="Number of trials")
    parser.add_argument('--ntrain',
                        type=int,
                        default=10,
                        help="Number of training iterations")
    parser.add_argument('--ntest',
                        type=int,
                        default=10,
                        help="Number of testing iterations")
    
    parsed, unknown = parser.parse_known_args()
    for arg in unknown:
        if arg.startswith(['-', '--']):
            parser.add_argument(arg,type=float)

    args = parser.parse_args()

    ## Load module
    module_path = os.path.abspath(os.path.join('..'))
    if module_path not in sys.path:
        sys.path.append(module_path)
    
    Model = getattr(importlib.import_module('bnn.scem'),
                    args.model)

    ## Load files
    file_list = glob.glob(f'../input/{args.input_dir}/mixed_*.npy')
    
    ## Output dirs
    output_dir = f'../output/{args.output_dir}'
    if not os.path.exists(output_dir):
        os.makedirs(output_dir, exist_ok=True)

    kargs = dict(vars(args))
    not_args = ['model', 'input_dir', 'output_dir', 'ntrial', 'ntrain', 'ntest']
    for k in not_args:
        kargs.pop(k, None)

    pool = Pool()
    pool.starmap(func, 
                 [(Model, kargs, seed, input_file, output_dir, trial_num, args.ntrain, args.ntest) 
                  for seed, (input_file, trial_num)
                  in enumerate(itertools.product(file_list, range(args.ntrial)))])

if __name__ == "__main__":
    main()