import input_mix

def main():
    input_mix.type1(
        "../input/bc",
        ["bassoon", "clarinet"])            
    return

if __name__ == "__main__":
    main()