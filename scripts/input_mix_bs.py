import input_mix

def main():
    input_mix.type1(
        "../input/bs",
        ["bassoon", "saxphone"])            
    return

if __name__ == "__main__":
    main()