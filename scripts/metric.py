import sys, os
import glob, re
import itertools
import numpy as np
import pandas as pd
import museval

def main():
    input_dir = f'../input/{sys.argv[1]}'
    output_dir = f'../output/{sys.argv[2]}'

    ref = np.load(f"{input_dir}/src.npy")
    nsrc = ref.shape[1]
    # The naming scheme of estimations is:
    #   est_{input_num}_{trial_num}
    file_list = glob.glob(f"{output_dir}/est_*.npy")

    input_nums = []
    trial_nums = []
    src_nums = []
    est_nums = []
    sirs = []
    for f in file_list:
        x = re.search('est_(.+?)_(.+?).npy', f)
        input_num = int(x[1])
        trial_num = int(x[2])
        
        est = np.load(f)
        est -= est.mean(0)
        est /= abs(est).max(0)
        nout = est.shape[1]
        
        for p in itertools.permutations(range(nout)):
            _, _, tmp, _, _ = museval.metrics.bss_eval(
                                ref.T, est.T[p,:],
                                compute_permutation=False,
                                window=np.inf)
            tmp = tmp.flatten()
            for i,(j,sir) in enumerate(zip(p,tmp)):
                input_nums.append(input_num)
                trial_nums.append(trial_num)
                src_nums.append(i)
                est_nums.append(j)
                sirs.append(sir)
        
    df = pd.DataFrame(zip(input_nums, trial_nums, src_nums, est_nums, sirs), 
                      columns=['input_num', 'trial_num', 'src', 'est', 'sir'])
    df.to_csv(f"{output_dir}/metric.csv", index=False)
    
if __name__ == "__main__":
    main()
