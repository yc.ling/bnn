import input_mix

def main():
    input_mix.type1(
        "../input/bv",
        ["bassoon", "violin"])            
    return

if __name__ == "__main__":
    main()