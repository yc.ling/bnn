import os
import glob, re

import pandas as pd
import numpy as np

def test2(x):
    return float(f"0.0{x}")

def test6(x):
    return float(f"0.{x}")

def func(n, model, params):
    df = pd.DataFrame()
    
    dirs = glob.glob(f"../output/{n}_*")
    for d in dirs:
        df2 = pd.read_csv(f"{d}/metric.csv")
        vals = re.findall(f'(?<=_)[A-Za-z0-9]+', d)
        for (pname, pfunc), v in zip(params, vals):
            df2[pname] = pfunc(v)
        df = df.append(df2)
    df['model'] = model
    return df

def main():
    df = pd.DataFrame()

    tests = [(1, 'T1k1', [('set', str)]),
             (2, 'T1k1', [('gamma', test2),
                          ('set', str)]),
             (3, 'T1k1', [('w_inh', float),
                          ('set', str)]),
             (4, 'T1k1', [('n_in', int),
                          ('set', str)]),
             (6, 'T6k1', [('use', test6),
                          ('set', str)]),
             (7, 'T6k1', [('tau_rec', float),
                          ('set', str)]),
             (8, 'T2k1', [('set', str)])
            ]
    
    for test in tests:
        df = df.append(func(*test))

    if not os.path.exists('../output/stats'):
        os.makedirs('../output/stats/')
    df.to_csv(f"../output/stats/metric.csv", index=False)
    
if __name__ == '__main__':
    main()